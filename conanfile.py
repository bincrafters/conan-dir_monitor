from conans import ConanFile, tools, os

class DirMonitorConan(ConanFile):
    name = "dir_monitor"
    version = "1.0.0"
    url = "https://github.com/bincrafters/conan-dir_monitor"
    description = "Please visit https://github.com/berkus/dir_monitor"
    license = "https://github.com/berkus/dir_monitor/blob/master/LICENSE_1_0.txt"
    source_url = "https://github.com/berkus/dir_monitor"
    requires = "Boost/1.64.0@conan/stable"
 
    def source(self):
        tools.get("{0}/archive/v{1}.tar.gz".format(self.source_url, self.version))
        
    def package(self):
        extracted_dir = "{0}-{1}".format(self.name, self.version)
        include_dir = os.path.join(extracted_dir, "include")
        self.copy(pattern="*", dst="include", src=include_dir)

    def package_id(self):
            self.info.header_only()

